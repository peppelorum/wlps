
from tastypie.resources import ModelResource
from tastypie import fields
from models import Show, Episode, Type

from tastypie.cache import SimpleCache
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS


class TypeResource(ModelResource):
    shows = fields.ToManyField('svtplayapi.api.TypeShowResource', 'show_set', full=False)

    class Meta:
        queryset = Type.objects.all()
        resource_name = 'category'
        excludes = ['html_class', ]
        cache = SimpleCache()
        ordering = [
            'title'
        ]

    def determine_format(self, request):
        return "application/json"

    def dehydrate(self, bundle):
        show_full = bundle.request.GET.get('full', "false").lower() == "true"
        if show_full:
            old_full = self.shows.full
            self.shows.full = True
            bundle.data['shows'] = self.shows.dehydrate(bundle)
            self.shows.full = old_full
        return bundle


class TypeShowResource(ModelResource):
    category = fields.ForeignKey(TypeResource, 'category')

    class Meta:
        queryset = Show.objects.all()
        resource_name = 'show'
        list_allowed_methods = ['get', ]
        filtering = {
            'title': ALL,
            'category': ALL_WITH_RELATIONS
        }
        cache = SimpleCache()

    def determine_format(self, request):
        return "application/json"


class ShowResource(ModelResource):
    episodes = fields.ToManyField(
        'svtplayapi.api.EpisodeResource',
        # 'episode_set',
        lambda bundle: Episode.objects.filter(http_status=200, show=bundle.obj),
        full=False,
        null=True)
    category = fields.ForeignKey(TypeResource, 'category', null=True)

    class Meta:
        queryset = Show.objects.filter(title__gt='')
        resource_name = 'show'
        list_allowed_methods = ['get', ]
        filtering = {
            'title': ALL,
            'category': ALL,
            'episodes': ALL_WITH_RELATIONS
        }
        ordering = [
            'title'
        ]
        cache = SimpleCache()

    def determine_format(self, request):
        return "application/json"

    def dehydrate(self, bundle):
        show_full = bundle.request.GET.get('full', "false").lower() == "true"
        if show_full:
            old_full = self.episodes.full
            self.episodes.full = True
            bundle.data['episodes'] = self.episodes.dehydrate(bundle)
            self.episodes.full = old_full
        return bundle


class EpisodeResource(ModelResource):
    show = fields.ForeignKey(ShowResource, 'show')

    class Meta:
        resource_name = 'episode'
        excludes = ['thumbnail', ]
        queryset = Episode.objects.filter(http_status=200)
        list_allowed_methods = ['get', ]
        filtering = {
            'kind_of': ALL,
            'url': ALL,
            'date_broadcasted': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'recommended': ALL,
            'show': ALL_WITH_RELATIONS,
            'viewable_in': ALL,
            'viewable_on_device': ALL,
            'http_status': ALL
        }
        ordering = [
            'date_broadcasted',
            'title'
        ]
        cache = SimpleCache()

    def determine_format(self, request):
        return "application/json"

    def build_schema(self):
        base_schema = super(EpisodeResource, self).build_schema()
        for f in self._meta.object_class._meta.fields:
            if f.name in base_schema['fields'] and f.choices:
                base_schema['fields'][f.name].update({
                    'choices': f.choices,
                    })
        return base_schema
