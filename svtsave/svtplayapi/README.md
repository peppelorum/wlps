# We Love Public Service - API mot SVT Play

SVT Play API är ett API som bygger på skrapad data från SVT Play. API:t  är ett REST-ish API som bara returnerar JSON. Det finns tre olika endpoints att tillgå, en för program, en för avsnitt och en för kategorier.