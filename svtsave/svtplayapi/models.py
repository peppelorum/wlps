# -*- coding: utf-8 -*-

import os
from django.db import models
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.utils.timezone import datetime, utc
from annoying.functions import get_config
import requests


class Show(models.Model):

    def thumbnail_upload_to(self, filename):
        return "shows/%s" % filename

    title = models.CharField(max_length=255, null=True)
    category = models.ForeignKey('Type', null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to=thumbnail_upload_to, max_length=255, blank=True, null=True)
    thumbnail_url = models.URLField(null=True, blank=True)

    def __unicode__(self):
        if self.title:
            return self.title
        return '<nada>'


class Episode(models.Model):

    def thumbnail_upload_to(self, filename):
        return "thumbnails/%s" % filename

    VIEWABLE_IN = (
        (1, 'Världen'),
        (2, 'Sverige')
    )

    VIEWABLE_ON_DEVICE = (
        (1, 'Mobile'),
        (2, 'No mobile')
    )

    KIND_OF = (
        (1, 'Episode'),
        (2, 'Clip')
    )

    title = models.CharField(max_length=255, null=True)
    title_slug = models.CharField(max_length=255, null=True, blank=True)
    full_url = models.URLField(max_length=255, null=True, blank=True)
    url = models.URLField(max_length=255)
    show = models.ForeignKey(Show, related_name='episode_set')
    viewable_in = models.IntegerField(choices=VIEWABLE_IN, default=1)
    viewable_on_device = models.IntegerField(choices=VIEWABLE_ON_DEVICE, default=1)
    kind_of = models.IntegerField(choices=KIND_OF, default=1)
    thumbnail_url = models.URLField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to=thumbnail_upload_to, max_length=255, blank=True, null=True)
    http_status = models.IntegerField(blank=True, null=True)
    http_status_checked_date = models.DateTimeField(null=True, blank=True)
    date_created = models.DateTimeField(null=True, blank=True)
    date_broadcasted = models.DateTimeField(null=True, blank=True)
    date_available_until = models.DateTimeField(null=True, blank=True)
    length = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    recommended = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.date_created = datetime.utcnow().replace(tzinfo=utc)
        super(Episode, self).save(*args, **kwargs)

    def update_http_status(self):
        try:
            req = requests.head(self.url)
            if req.status_code == 404:
                self.http_status = 404

        except requests.exceptions.InvalidURL as inst:
            print 'error', inst
            self.http_status = -1

        except requests.exceptions.ConnectionError as inst:
            print 'error', inst

        self.http_status_checked_date = datetime.utcnow().replace(tzinfo=utc)
        self.save()

    def __unicode__(self):
        if self.title is None:
            return '<nada>'
        return self.title


class Type(models.Model):

    def thumbnail_upload_to(self, filename):
        return "types/%s" % filename

    title = models.CharField(max_length=100, blank=True, null=True)
    html_class = models.CharField(max_length=100)
    url = models.URLField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to=thumbnail_upload_to, max_length=255, blank=True, null=True)
    thumbnail_url = models.URLField(null=True, blank=True)

    def __unicode__(self):
        return unicode(self.title)
