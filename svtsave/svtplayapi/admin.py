
from django.contrib import admin
from models import Show, Episode, Type
from django.conf.urls.defaults import patterns
from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext

admin.site.register(Show)
admin.site.register(Type)

class EpisodeAdmin(admin.ModelAdmin):
    list_filter = ('http_status', 'recommended', 'show')

admin.site.register(Episode, EpisodeAdmin)


# def my_view(request):
#     data = {
#         'list': Episode.objects.filter(state=4, http_status=200)
#     }
#     return render_to_response('core/admin/admin.html',
#                               data,
#                               context_instance=RequestContext(request))
# #    return HttpResponse("Hello!")
#
#
# def get_admin_urls(urls):
#     def get_urls():
#         my_urls = patterns('',
#                            (r'^my_view/$', admin.site.admin_view(my_view))
#         )
#         return my_urls + urls
#     return get_urls
#
# admin_urls = get_admin_urls(admin.site.get_urls())
# admin.site.get_urls = admin_urls