from django.conf.urls import patterns, include, url

from tastypie.api import Api

from django.contrib import admin
admin.autodiscover()

from svtplayapi.api import ShowResource, EpisodeResource, TypeResource

show_resource = ShowResource()

v1_api = Api(api_name='v1')
v1_api.register(ShowResource())
v1_api.register(EpisodeResource())
v1_api.register(TypeResource())

urlpatterns = patterns('',
    (r'', include(v1_api.urls)),
    # url(r'api/doc/', include('tastypie_swagger.urls', namespace='tastypie_swagger')),
)
