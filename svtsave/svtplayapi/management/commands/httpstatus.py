from django.core.management.base import BaseCommand, CommandError
import requests
from svtplayapi.models import Show, Episode, Type

#from datetime import datetime
from django.utils.timezone import datetime, timedelta, make_aware
from pytz import timezone, utc

from annoying.functions import get_config

try:
    from johnny.cache import enable
    #Enables the Johnny Cache mechanism
    enable()
except ImportError:
    pass


class HttpStatus():

    def __init__(self):

        episodes = Episode.objects.filter(http_status=200)

        tot = episodes.count()

        print 'Total: ', tot

        i = 1

        for episode in episodes:

            if i % 100 == 0:
                print i

            i += 1

            tz = timezone(get_config('TIME_ZONE', None))
            hoj = datetime.utcnow().replace(tzinfo=tz)

            r = requests.head(episode.url)
            episode.http_status_checked_date = hoj
            episode.http_status = r.status_code
            episode.save()

        print '******'
        print 'KLART!'
        print '******'


class Command(BaseCommand):

    def handle(self, *args, **options):
        HttpStatus()
