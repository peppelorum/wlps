# -*- coding: utf-8 -*-


from django.core.management.base import BaseCommand
from django.utils.timezone import datetime, timedelta, make_aware
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from pytz import timezone, utc
from annoying.functions import get_config
from pyquery import PyQuery
import requests
from svtcrawler import SvtCrawler


try:
    from johnny.cache import enable
    #Enables the Johnny Cache mechanism
    enable()
except ImportError:
    pass

from svtplayapi.models import Show, Episode, Type


def shellquote(s):
    s = s.replace('/', '-')
    valid_chars = u' -_.():0123456789abcdefghijklmnpoqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVZXYZÅÄÖ'
    s = ''.join(c for c in s if c in valid_chars)
    s = s.strip()

    return s


def numerics(s):
    n = 0
    for c in s:
        if not c.isdigit():
            return n
        else:
            n = n * 10 + int(c)
    return n

#
# def sanitize_description(value):
#     soup = PyQuery(value)
#     soup = soup.remove('span.playMetaText')
#     soup.remove('span.playMetaText')
#     soup.remove('time')
#     soup.remove('strong')
#
#     return soup.html().split('<span>')[-1:][0].replace('</span>', '')


def parse_date(datum, typ):
    datum = unicode(datum)
    tz = timezone(get_config('TIME_ZONE', None))
    hoj = datetime.utcnow().replace(tzinfo=tz)
    if datum.find('dag') != -1:
        days = numerics(datum)
        if typ == '+':
            ret = hoj + timedelta(days=days)
        else:
            ret = hoj - timedelta(days=days)
    elif datum.find('tim') != -1:
        hours = numerics(datum)
        if typ == '+':
            ret = hoj + timedelta(hours=hours)
        else:
            ret = hoj - timedelta(hours=hours)
    else:
        return hoj

    return ret

#
# class SvtCrawler():
#
#     def __init__(self):
#         self.timezone = get_config('TIME_ZONE', 'Europe/Stockholm')
#
#         print '*********'
#         print 'CRAWLING!'
#         print '*********'
#
#         self.baseurl = 'http://www.svtplay.se'
#         self.url = 'http://www.svtplay.se/program'
#         self.category_url = 'http://www.svtplay.se%s/?tab=titles&sida=100'
#         self.newEpisodesToday = 0
#
#         page = PyQuery(self.url)
#
#         for link in page.find("a.playCategoryLink"):
#             py_link = PyQuery(link)
#             href = py_link.attr('href')
#             type_ = href.split('/')[-1:][0]
#             text = py_link.text()
#             img = self.baseurl + PyQuery(link).find('img').attr('src')
#             type_obj, created = Type.objects.get_or_create(html_class=type_)
#
#             if created:
#                 thumbnail_req = requests.get(img)
#                 type_obj.url = self.category_url % href
#                 type_obj.title = text
#                 type_obj.thumbnail_url = img
#                 img_temp = NamedTemporaryFile(delete = True)
#                 img_temp.write(thumbnail_req.content)
#                 img_temp.flush()
#                 type_obj.thumbnail.save("image_%s.jpg" % type_obj.pk, File(img_temp))
#                 type_obj.save()
#
#         # Index categories
#         for link in page.find("a.playCategoryLink"):
#             py_link = PyQuery(link)
#             href = py_link.attr('href')
#
#             # Index all shows
#             pq_show = PyQuery(self.category_url % href)
#             for article_iter in pq_show.find('article.svtUnit'):
#                 article = PyQuery(article_iter)
#                 url = article.find('a.playLink').attr('href')
#
#                 if url and url.find('/video/') == -1:
#                     show_url = self.baseurl + url
#                     show_title = article.text()
#                     thumbnail_url = article.find('img.playGridThumbnail').attr('src').replace('medium', 'large')
#
#                     if thumbnail_url.find('http') == -1:
#                         thumbnail_url = self.baseurl + thumbnail_url
#
#                     show, created = Show.objects.get_or_create(url=show_url)
#                     if created:
#                         show.title = show_title
#                         show.category = type_obj
#                         show.thumbnail_url = thumbnail_url
#                         thumbnail_req = requests.get(thumbnail_url)
#                         img_temp = NamedTemporaryFile(delete = True)
#                         img_temp.write(thumbnail_req.content)
#                         img_temp.flush()
#                         show.thumbnail.save("image_%s.jpg" % type_obj.pk, File(img_temp))
#                         show.save()
#
#                     episodes_url = show_url + '?tab=episodes&sida=1000'
#                     clip_url = show_url + '?tab=clips&sida=1000'
#
#                     # Index both episodes and clips
#                     self.crawl(episodes_url, show, 1)
#                     self.crawl(clip_url, show, 2)
#
#         self.updateRecommended()
#
#         print 'New ones:', self.newEpisodesToday
#
#     def updateRecommended(self):
#         self.recUrl = 'http://www.svtplay.se/?tab=recommended&sida=100'
#         page = PyQuery(self.recUrl)
#         Episode.objects.all().update(recommended=False)
#
#         for article in page('[data-tabname="recommended"] article.svtUnit'):
#             a = PyQuery(article)
#             url = self.baseurl + a.find('a.playLink').attr('href')
#             try:
#                 episode = Episode.objects.get(url=url)
#                 episode.recommended = True
#                 episode.save()
#             except Episode.DoesNotExist:
#                 pass
#             except Episode.MultipleObjectsReturned:
#                 pass
#
#     def crawl(self, base_url, show, kind_of):
#         a = PyQuery(base_url)
#
#         for article_iter in a.find('article.svtUnit'):
#             article = PyQuery(article_iter)
#             episode = article.find('a.playLink')
#             full_url = self.baseurl + article.find('a.playLink').attr('href')
#
#             #Check if the url contains an extra /Random-Title, if so, remove it
#             if len(full_url.split('/')) == 6:
#                 url = full_url.rpartition('/')[0]
#             else:
#                 url = full_url
#
#             broadcasted = article.find('time').attr('datetime')
#
#             try:
#                 Episode.objects.get(url=url)
#                 continue
#             except Episode.DoesNotExist:
#                 pass
#             except Episode.MultipleObjectsReturned:
#                 pass
#
#             if url.find('video') != -1 and len(broadcasted) > 1:
#
#                 available = parse_date(article.attr('data-available'), '+')
#                 length = article.attr('data-length')
#
#                 if not episode.attr('href').startswith('http'):
#
#                     try:
#                         episode, created = Episode.objects.get_or_create(
#                             show=show,
#                             url=url,
#                         )
#
#                         if created:
#
#                             article_full = PyQuery(url)
#                             thumbnail = article_full.find('img.svtHide-No-Js').eq(0).attr('data-imagename')
#                             thumbnail_req = requests.get(thumbnail)
#                             meta = article_full.find('.playVideoMetaInfo div')
#
#                             desc = article_full.find('.playVideoInfo p').text()
#                             if len(desc) == 0:
#                                 desc = article_full.find('.playVideoInfo span')
#                             desc = sanitize_description(unicode(desc))
#
#                             if str(meta).find('Kan endast ses i Sverige') == -1:
#                                 rights = 1
#                             else:
#                                 rights = 2
#
#                             if str(meta).find('Kan ses i mobilen') > -1:
#                                 on_device = 1
#                             else:
#                                 on_device = 2
#
#                             soupEpisode = PyQuery(url)
#                             episodeTitle = soupEpisode.find('title').eq(0).text().replace('| SVT Play', '')
#                             episode.title = episodeTitle
#                             episode.title_slug = shellquote(episodeTitle)
#                             episode.http_status = 200
#                             episode.http_status_checked_date = datetime.utcnow().replace(tzinfo=utc)
#                             episode.date_available_until = available
#                             episode.date_broadcasted = broadcasted
#                             episode.length = length
#                             episode.description = desc
#                             episode.viewable_on_device = on_device
#                             episode.viewable_in = rights
#                             episode.kind_of = kind_of
#                             episode.thumbnail_url = thumbnail
#
#                             # Save image
#                             img_temp = NamedTemporaryFile(delete=True)
#                             img_temp.write(thumbnail_req.content)
#                             img_temp.flush()
#
#                             episode.thumbnail.save("image_%s.jpg" % episode.pk, File(img_temp))
#                             episode.save()
#
#                             self.newEpisodesToday += 1
#
#                         print '\tNew', created, episode, episode.id
#
#                     except Exception as inst:
#                         print 'APA', inst
#                         print 'url', url


class Command(BaseCommand):

    def updateRecommended(self):
        recUrl = 'http://www.svtplay.se/?tab=recommended&sida=100'
        baseurl = 'http://www.svtplay.se'
        page = PyQuery(recUrl)
        Episode.objects.all().update(recommended=False)

        for article in page('[data-tabname="recommended"] article.svtUnit'):
            a = PyQuery(article)
            url = baseurl + a.find('a.playLink').attr('href')
            try:
                episode = Episode.objects.get(url=url)
                episode.recommended = True
                episode.save()
            except Episode.DoesNotExist:
                pass
            except Episode.MultipleObjectsReturned:
                pass

    def handle(self, *args, **options):
        self.crawl = SvtCrawler(max_timestamp=None, min_timestamp=None, skip_urls=Episode.objects.all().values_list('url', flat=True))

        for category in self.crawl.categories:

            if category.title == u'Öppet arkiv':
                continue

            # print unicode(category.title)

            type_obj, created = Type.objects.get_or_create(html_class=category.html_class)

            # print unicode(type_obj), created

            if created:
                thumbnail_req = requests.get(category.thumbnail)
                type_obj.url = category.url
                type_obj.title = category.title
                type_obj.thumbnail_url = category.thumbnail
                # img_temp = NamedTemporaryFile(delete = True)
                # img_temp.write(thumbnail_req.content)
                # img_temp.flush()
                # type_obj.thumbnail.save("image_%s.jpg" % type_obj.pk, File(img_temp))
                type_obj.save()

            for show_api in category.shows:
                show, created = Show.objects.get_or_create(url=show_api.url)

                # print unicode(show_api.title)
                # print unicode(created)

                if created:
                    show.title = show_api.title
                    show.category = type_obj
                    show.thumbnail_url = show_api.thumbnail
                    thumbnail_req = requests.get(show_api.thumbnail)
                    # img_temp = NamedTemporaryFile(delete = True)
                    # img_temp.write(thumbnail_req.content)
                    # img_temp.flush()
                    # show.thumbnail.save("image_%s.jpg" % type_obj.pk, File(img_temp))
                    show.save()

                for episode_api in show_api.episodes:
                    episode, created = Episode.objects.get_or_create(
                        show=show,
                        url=episode_api.url
                    )

                    # print episode_api.title, episode_api.url, episode_api.kind_of
                    # from pprint import pprint
                    # pprint(vars(episode_api))

                    if created:
                        if episode_api.kind_of == 'episodes':
                            kind_of = 1
                        else:
                            kind_of = 2

                        episode.title = episode_api.title
                        episode.title_slug = shellquote(episode_api.title)
                        episode.http_status = 200
                        episode.http_status_checked_date = datetime.utcnow().replace(tzinfo=utc)
                        episode.date_available_until = episode_api.date_available_until
                        episode.date_broadcasted = episode_api.date_broadcasted
                        episode.length = episode_api.length
                        episode.description = episode_api.description
                        episode.viewable_on_device = episode_api.viewable_on_device
                        episode.viewable_in = episode_api.viewable_in
                        episode.kind_of = kind_of
                        episode.thumbnail_url = episode_api.thumbnail_url
                        episode.save()

                for episode_api in show_api.clips:
                    episode, created = Episode.objects.get_or_create(
                        show=show,
                        url=episode_api.url
                    )

                    # print unicode(episode_api.title), unicode(episode_api.url), episode_api.kind_of
                    # from pprint import pprint
                    # pprint(vars(episode_api))

                    if created:
                        if episode_api.kind_of == 'episodes':
                            kind_of = 1
                        else:
                            kind_of = 2

                        episode.title = episode_api.title
                        episode.title_slug = shellquote(episode_api.title)
                        episode.http_status = 200
                        episode.http_status_checked_date = datetime.utcnow().replace(tzinfo=utc)
                        episode.date_available_until = episode_api.date_available_until
                        episode.date_broadcasted = episode_api.date_broadcasted
                        episode.length = episode_api.length
                        episode.description = episode_api.description
                        episode.viewable_on_device = episode_api.viewable_on_device
                        episode.viewable_in = episode_api.viewable_in
                        episode.kind_of = kind_of
                        episode.thumbnail_url = episode_api.thumbnail_url

                        # Save image
                        # img_temp = NamedTemporaryFile(delete=True)
                        # img_temp.write(thumbnail_req.content)
                        # img_temp.flush()

                        # episode.thumbnail.save("image_%s.jpg" % episode.pk, File(img_temp))
                        episode.save()


        self.updateRecommended()
        self.stdout.write('DONE!')