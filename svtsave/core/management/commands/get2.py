from django.core.management.base import BaseCommand, CommandError
from core.models import Show, Episode, Type

import os
import requests
from bs4 import BeautifulSoup

from itertools import chain

from core.tasks import download
from annoying.functions import get_config

class SvtGet():

    def __init__(self):

        self.episodes = Episode.objects.filter(state=0, http_status=200).exclude(show__profile=None)
        self.episodesPreqeued = Episode.objects.filter(state=4, http_status=200)
        result_list = list(chain(self.episodes, self.episodesPreqeued))

#        print 'apor', result_list

        for episode in result_list:
            print 'GETTING: ', str(episode.id)
            print 'URL: ', str(episode.url)
            download.delay(episode.id, episode.url, get_config('SVTGETSAVEFOLDER', os.path.join(get_config('PROJECT_DIR', 'FAILED'), 'episodes')), str(episode.id))
#            break



class Command(BaseCommand):

    def handle(self, *args, **options):
        if args != ():
            episode = Episode.objects.get(id=args[0])
            print 'GETTING: ', str(episode.id)
            print 'URL: ', str(episode.url)
            download.delay(episode, get_config('SVTGETSAVEFOLDER', os.path.join(get_config('PROJECT_DIR', 'FAILED'), 'episodes')), str(episode.id))
        else:
            self.crawl = SvtGet()
            self.stdout.write('Yay!')


        self.stdout.write(get_config('SVTGETSAVEFOLDER', 'noo'))