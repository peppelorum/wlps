from django.core.management.base import BaseCommand, CommandError
from core.models import Show, Episode, Type

import os
import requests
from bs4 import BeautifulSoup
from sys import stdout
#from datetime import datetime, timedelta
from pytz import timezone, utc
#from django.utils.timezone import utc
from django.utils.timezone import datetime, timedelta, make_aware
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from johnny.cache import enable
from annoying.functions import get_config
from pyquery import PyQuery

print dir(datetime)


from libs.svtget import Pirateget
from core.tasks import download
from core.models import Episode


#Enables the Johnny Cache mechanism
enable()


def numerics(s):
    n = 0
    for c in s:
        if not c.isdigit():
            return n
        else:
            n = n * 10 + int(c)
    return n


def stripAllTags( html ):
    if html is None:
        return None
    return ''.join( BeautifulSoup( html ).findAll( text = True ) )


def parse_date(grej, typ):
    grej = unicode(grej)
    tz = timezone(get_config('TIME_ZONE', None))
    hoj = datetime.utcnow().replace(tzinfo=tz)
    if grej.find('dag') != -1:
        days = numerics(grej)
        if typ == '+':
            ret = hoj + timedelta(days=days)
        else:
            ret = hoj - timedelta(days=days)
    elif grej.find('tim') != -1:
        hours = numerics(grej)
        if typ == '+':
            ret = hoj + timedelta(hours=hours)
        else:
            ret = hoj - timedelta(hours=hours)
    else:
        return hoj

    return ret


class SvtCrawler():

    def __init__(self):


#        from sys import stdout
#        from time import sleep
#        for i in range(1,20):
##            stdout.write("\r")
#            stdout.write(str(i))
#            stdout.flush()
#            sleep(1)
#        stdout.write("\n") # move the cursor to the next line

#        print 'HTTP Update'
#        episodes = Episode.objects.filter(http_status=200)
#        for episode in episodes:
#            episode.update_http_status()
#            stdout.write('.')
#            stdout.flush()

        self.timezone = get_config('TIME_ZONE', 'Europe/Stockholm')

        print '*********'
        print 'CRAWLING!'
        print '*********'

        Episode.objects.all().update(recommended=False)

        self.baseurl = 'http://www.svtplay.se'
        self.url = 'http://www.svtplay.se/program'
        self.recUrl = 'http://www.svtplay.se/?tab=recommended&sida=100'
        page = PyQuery(self.recUrl)

        for article in page('[data-tabname="recommended"] article.svtUnit'):
            a = PyQuery(article)
            url = self.baseurl + a.find('a.playLink').attr('href')
            try:
                episode = Episode.objects.get(url=url)
                episode.recommended = True
                episode.save()
            except Episode.DoesNotExist:
                pass
            except Episode.MultipleObjectsReturned:
                pass

        print '*************'
        print 'Recommendations updated'
        print '*************'

        r = requests.get(self.url)
        soup = BeautifulSoup(r.content)

        newEpisodesToday = 0

        # Loop through all shows
        for li in soup.select("li.playListItem"):

            item = li.a
            type_ = [e for e in li.attr('class') if e.startswith('playJsAlphabeticCategory')][0]

            show_url = self.baseurl + item['href'] +'?tab=episodes&sida=1000'
            clip_url = self.baseurl + item['href']+ '?tab=clips&sida=1000'
            a = PyQuery(show_url)

            try:
                type_title =  type_.split('_')[1].strip()
            except:
                type_title = '<nada>'

            type_obj, created = Type.objects.get_or_create(html_class=type_)
            show, created = Show.objects.get_or_create(title=item.text, type=type_obj)

            for article_iter in a.find('article.svtUnit'):
                article = PyQuery(article_iter)
                episode = article.find('a.playLink')

                url = self.baseurl + article.find('a.playLink').attr('href')

                try:
                    Episode.objects.get(url=url)
                    print url
                    print 'exists'
                    continue
                except Episode.DoesNotExist:
                    pass
                except Episode.MultipleObjectsReturned:
                    pass

                available = parse_date(article.attr('data-available'), '+')
                length = article.attr('data-length')
                broadcasted = article.find('time').attr('datetime')
                thumbnail = article.find('img.playGridThumbnail').attr('src')
                if thumbnail.find('http') == -1:
                    thumbnail = self.baseurl + thumbnail
                thumbnail_req = requests.get(thumbnail)

                if url.find('video') != -1:

                    if not episode.attr('href').startswith('http'):

                        try:
                            req2 = requests.get(url)
                            soupEpisode = BeautifulSoup(req2.content)

#                            Here the description should be somewhere
                            desc = soupEpisode.select('.playVideoInfo p')

                            episodeTitle = soupEpisode.title.text.replace('| SVT Play', '')

                            episode, created = Episode.objects.get_or_create(
                                show=show,
                                url=url,
                            )

                            if created:
                                episode.title = episodeTitle
                                episode.http_status = 200
                                episode.http_status_checked_date = datetime.utcnow().replace(tzinfo=utc)
                                episode.date_available_until = available
                                episode.date_broadcasted = broadcasted
                                episode.length = length

                                newEpisodesToday += 1

                            print 'apor', created, episode, episode.id

                            # Save image
                            img_temp = NamedTemporaryFile(delete = True)
                            img_temp.write(thumbnail_req.content)
                            img_temp.flush()

                            episode.thumbnail.save("image_%s.jpg" % episode.pk, File(img_temp))
                            episode.save()
                        except Exception as inst:
                            print 'APA', inst
                            print 'url', show_url

                # break
            #
            # break

        print '*************'
        print 'New episodes today:', newEpisodesToday
        print '*************'




class Command(BaseCommand):

    def handle(self, *args, **options):
        self.crawl = SvtCrawler()
        self.stdout.write('DONE!')