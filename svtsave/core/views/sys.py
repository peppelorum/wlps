# Create your views here.

from django.views import generic
from django.shortcuts import get_object_or_404, redirect, HttpResponse, HttpResponseRedirect
# from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core import serializers


from notifications import notify
from annoying.functions import get_config
import boto

from core.decorators import patch_view_decorator
from core.utils import reverse
from django.views.decorators.cache import cache_page
login_required = patch_view_decorator(login_required)
#cache_page = patch_view_decorator(cache_page)


from core.models import Profile, Episode, Show, UserDownloadedShows
from core.forms import SubscriptionForm

@login_required
class ProfileDetailView(generic.DetailView):
    model = Profile

    def get_object(self, queryset=None):
        return get_object_or_404(Profile, user=self.request.user)

    def get_queryset(self):
        return get_object_or_404(Profile, user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        context['show_list'] = Show.objects.filter(profile__user=self.request.user)
        print dir(self.object.user)
        print self.object.user.userdownloadedshows_set.all()
        context['queue_list'] = self.object.user.userdownloadedshows_set.all().order_by('episode__date_downloaded')
        return context
#
#class StuffListView(generic.ListView):
#    model = Profile

class StuffCreateView(generic.CreateView):
    model = Profile
    form_class = SubscriptionForm

    def get_form(self, form_class):
        form = super(StuffCreateView,self).get_form(form_class) #instantiate using parent
        form.request = self.request
#        form.fields['show'].queryset = Subscription.objects.filter(user=self.request.user)
#        print form.fields
        return form

@login_required
class ProfileUpdateView(generic.UpdateView):
    model = Profile
    form_class = SubscriptionForm

    def get_object(self, queryset=None):
        return get_object_or_404(Profile, user=self.request.user)

@login_required
class EpisodeListView(generic.ListView):
    model = Episode

@login_required
class EpisodeDetailView(generic.DetailView):
    model = Episode

    def get_context_data(self, **kwargs):
        context = super(EpisodeDetailView, self).get_context_data(**kwargs)
        self.object.users_to_notify()
        return context





#@class_view_decorator(login_required)
@login_required
class ShowListView(generic.ListView):
    model = Show
    queryset = Show.objects.all().order_by('title')

@login_required
class ShowDetailView(generic.DetailView):
    model = Show

    def get_context_data(self, **kwargs):
        context = super(ShowDetailView, self).get_context_data(**kwargs)
        context['episode_list'] = Episode.objects.filter(show=self.get_object())
        return context

@login_required
class EpisodePrequeue(generic.RedirectView):

    def get_redirect_url(self, pk):
        ep = get_object_or_404(Episode, pk=pk)
        ep.state = 4
        ep.save()

        queued = UserDownloadedShows.objects.get_or_create(episode=ep, user=self.request.user)

        return self.request.GET.get('next', '/shows/')

@login_required
class EpisodeRequeue(generic.RedirectView):

    def get_redirect_url(self, pk):
        ep = get_object_or_404(Episode, pk=pk)
        ep.state = 4
        ep.save()
        return self.request.GET.get('next', '/shows/')
#
# @login_required
# class EpisodeDownloadView(generic.RedirectView):
#
#     def get_redirect_url(self, pk):
#         print 1
#         ep = get_object_or_404(Episode, pk=pk)
#         if ep.check_if_downloaded_file_exists():
#             print 2
#             return ep.episode_link()
#         else:
#             print 3
#             return reverse('ep_requeue', args=(ep.id, ))


# @login_required()
def EpisodeDownloadView(request, pk):

    from django.utils import simplejson

    print 1
    ep = get_object_or_404(Episode, pk=pk)
    file_exists = ep.check_if_downloaded_file_exists()

    if file_exists:
        # generate_url(expires_in, method='GET', headers=None, force_http=False, response_headers=None, expires_in_absolute=False)
        # get_key(key_name, headers=None, version_id=None, response_headers=None, generation=None, meta_generation=None)
        # url =

        BUCKET = get_config('BUCKET', '')
        GOOGLE_STORAGE = get_config('GOOGLE_STORAGE', '')
        GS_KEY = get_config('GS_KEY', '')
        GS_SECRET = get_config('GS_SECRET', '')

        # uri = boto.storage_uri(BUCKET, GOOGLE_STORAGE)
        # conn = uri.connect(GS_KEY, GS_SECRET)

        from boto.s3.connection import S3Connection
        conn = S3Connection(GS_KEY, GS_SECRET)

        bucket = conn.get_bucket('wlps')

        key = bucket.lookup('%s.mp4' % str(ep.id))
        # key.make_public()

        # dst_uri = boto.storage_uri(
        #     BUCKET + '/' + str(ep.id) + '.mp4', GOOGLE_STORAGE)
        #
        #
        # key = dst_uri.get_key()

        # f = open('torrentfile.torrent', 'w')

        print key
        # print key.get_torrent_file(f)
        #
        # f.close()
        print dir(key)
        # print key.generate_url(3000)
        # print dir(dst_uri)
        # print key.generate_url(4000, response_headers={'content-type': 'application/octet-stream', 'Content-Disposition': 'attachment; filename="fname.ext"' })




    if request.is_ajax:
        if file_exists:
            return HttpResponse(simplejson.dumps({"Exists": 'Yes', 'Link': ep.episode_link()}), content_type="application/json")
        else:
            ep.pre_queue()
            return HttpResponse(simplejson.dumps({"Exists": 'No'}), content_type="application/json")
    else:
        if file_exists:
            print 2
            return HttpResponseRedirect(ep.episode_link())
        else:
            print 3
            # return reverse('ep_requeue', args=(ep.id, ))


@login_required
class StatusView(generic.TemplateView):
    template_name = 'core/status.html'

    def get_context_data(self, **kwargs):
        context = super(StatusView, self).get_context_data(**kwargs)
        context['episodes_prequeued'] = Episode.objects.filter(state=4)
        context['episodes_queued'] = Episode.objects.filter(state=1, http_status=200).exclude(show__profile=None)
        context['episodes_downloading'] = Episode.objects.filter(state=2, http_status=200).exclude(show__profile=None)
        return context



@csrf_exempt
def EpisodeCallback(request):

    req_id = request.POST.get('id')
    if not req_id and settings.DEBUG:
        req_id = request.GET.get('id')

    obj = get_object_or_404(Episode, id=req_id)

    if obj:
        obj.state = 3
        obj.save()

        profiles = obj.users_to_notify()

        for profile in profiles:

            # notify.send(obj, recipient=profile.user, verb='is downloaded', action_object=obj, actions=[serializers.serialize("json", [obj])])
            # notify.send(obj, recipient=profile.user, verb='is downloaded', action_object=obj, href='asd', title='aertert')
            notify.send(obj, recipient=profile.user, verb='is downloaded')

        print profiles

    return HttpResponse('Yay!')


from django.contrib.syndication.views import Feed
from notifications.models import Notification

class LatestEntriesFeed(Feed):
    title = "WLPS Your personal torrent feed"
    link = "http://sys.welovepublicservice.se/"
    # description = ""

    def get_object(self, request, key):
        return get_object_or_404(Profile, unique=key)

    def items(self, obj):
        return Notification.objects.filter(recipient=obj.user, actor_content_type__name='episode')

    def item_title(self, item):
        # return item.actor
        return '%s - %s' % (item.actor.show, item.actor)

    def item_description(self, item):
        return item.description
        # return '%s - %s' % (item.show, item.description)

    def item_link(self, item):
        try:
            return item.actor.episode_link_torrent()
        except:
            pass