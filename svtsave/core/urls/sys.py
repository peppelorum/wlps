

from django.conf.urls import patterns, url, include

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


from core.views import sys as views


import notifications

urlpatterns = patterns(
    '',
    url('^inbox/notifications/', include(notifications.urls)),
    url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/shows/'}, name='redirthingie'),

    url(
       regex=r"^profile/create/$",
       view=views.StuffCreateView.as_view(),
       name="stuff_create",
       ),
    url(
       regex=r"^profile/edit/$",
       view=views.ProfileUpdateView.as_view(),
       name="profile_update",
       ),
    url(
       regex=r"^profile/$",
       view=views.ProfileDetailView.as_view(),
       name="profile_detail",
       ),
    url(
       regex=r"^shows/$",
       view=views.ShowListView.as_view(),
       name="show_list",
       ),
    url(
       regex=r"^episodes/$",
       view=views.EpisodeListView.as_view(),
       name="ep_list",
       ),
    url(
       regex=r"^episode/(?P<pk>\d+)$",
       view=views.EpisodeDetailView.as_view(),
       name="ep_detail",
       ),
    # url(
    #    regex=r"^episode/get/(?P<pk>\d+)?$",
    #    view=views.EpisodeDownloadView.as_view(),
    #    name="ep_get",
    #    ),
    url(
        regex=r"^episode/get/(?P<pk>\d+)?$",
        view=views.EpisodeDownloadView,
        name="ep_get",
        ),
    url(
       regex=r"^show/(?P<pk>\d+)$",
       view=views.ShowDetailView.as_view(),
       name="show_detail",
       ),

    url(
       regex=r"^status/$",
       view=views.StatusView.as_view(),
       name="status",
       ),

    url(
       regex=r"^episode/queue/(?P<pk>\d+)$",
       view=views.EpisodePrequeue.as_view(),
       name="ep_queue",
    ),

    url(
       regex=r"^episode/requeue/(?P<pk>\d+)$",
       view=views.EpisodeRequeue.as_view(),
       name="ep_requeue",
    ),

    url(
        regex=r"^episode/callback/$",
        view=views.EpisodeCallback,
        name="ep_callback",
    ),

    url(
        regex=r'^feed/(?P<key>[a-z0-9]+)/$',
        view=views.LatestEntriesFeed(),
        name="feed_unique"
    ),

)

urlpatterns += patterns('',
                        # Uncomment the admin/doc line below to enable admin documentation:
                        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                        # Uncomment the next line to enable the admin:
                        url(r'^admin/', include(admin.site.urls)),

                        url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
                        url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
                        )