from django.conf.urls import patterns, include, url

from core.views.sys import EpisodeCallback

from tastypie.api import Api




urlpatterns = patterns('',
    url(r'^$', 'django.views.generic.simple.redirect_to', {'url': 'https://plus.google.com/111310534587045706721/posts/'}, name='awaaaaay'),


    url(
        regex=r"^episode/callback/$",
        view=EpisodeCallback,
        name="ep_callback",
        ),
)



