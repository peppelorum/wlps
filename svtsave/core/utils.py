# -*- coding: utf-8 -*-


import os
from django.core.urlresolvers import reverse as django_reverse

def reverse(viewname, urlconf=None, args=None, kwargs=None, prefix=None, current_app=None):
    """
    Wrapper of django.core.urlresolvers.reverse that attaches arguments in kwargs as query string parameters
    """
    if kwargs:
        return '%s?%s' % (django_reverse(viewname, urlconf, args, None, prefix, current_app), \
                          '&'.join(['%s=%s' % (k,v) for k,v in kwargs.items()]))
    else:
        return django_reverse(viewname, urlconf, args, kwargs, prefix, current_app)


def random_url():
    return os.urandom(16).encode('hex')

def shellquote(s):
    s = s.replace('/', '-')
    valid_chars = u' -_.():0123456789abcdefghijklmnpoqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVZXYZÅÄÖ'
    s = ''.join(c for c in s if c in valid_chars)
    s = s.strip()

    return s