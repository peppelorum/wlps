

from django import forms
from models import Profile


class SubscriptionForm(forms.ModelForm):

    class Meta:
        model = Profile
        exclude = (
            'user',
        )

#    def __init__(self, *args, **kwargs):
#        self.request = kwargs.pop('request', None)
#        super(SubscriptionForm, self).__init__(*args, **kwargs)
#
#    def save(self, commit=True):
#        issue = super(SubscriptionForm, self).save(commit=False)
#        issue.user = self.request.user
#        issue.save()