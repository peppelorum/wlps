from django.utils.decorators import method_decorator
from inspect import isfunction

class _cbv_decorate(object):
    def __init__(self, dec):
        self.dec = method_decorator(dec)

    def __call__(self, obj):
        obj.dispatch = self.dec(obj.dispatch)
        return obj

def patch_view_decorator(dec):
    def _conditional(view):
        if isfunction(view):
            return dec(view)

        return _cbv_decorate(dec)(view)

    return _conditional

#from django.utils.decorators import method_decorator

#def class_view_decorator(function_decorator):
#    """Convert a function based decorator into a class based decorator usable
#    on class based Views.
#
#    Can't subclass the `View` as it breaks inheritance (super in particular),
#    so we monkey-patch instead.
#    """
#
#    def simple_decorator(View):
#        View.dispatch = method_decorator(function_decorator)(View.dispatch)
#        return View
#
#    return simple_decorator