# -*- coding: utf-8 -*-

import os
#from datetime import datetime, time
from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from annoying.functions import get_config
import requests
from itertools import chain

#from django.utils.timezone import utc

from django.utils.timezone import datetime, timedelta, make_aware, utc


from svtplayapi.models import Show, Episode

class Profile(models.Model):
    user = models.OneToOneField(User, blank=True, unique=True)
    shows = models.ManyToManyField(Show, blank=True, null=True)
    unique = models.CharField(max_length=32, db_index=True, default=os.urandom(16).encode('hex'))



    def get_absolute_url(self):
        return '/profile/'

    def get_feed_url(self):
        return reverse('feed_unique', args=(self.unique, ))


class UserDownloadedShows(models.Model):
    user = models.ForeignKey(User)
    episode = models.ForeignKey(Episode)
#    download_date = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return '%s - %s' % (self.episode.title, self.user)

    def is_downloaded(self):
        if self.episode.state == 3:
            return True
        else:
            return False

# class Notificaton

#class Subscription(models.Model):
#    show = models.ForeignKey(Show)
#    user = models.ForeignKey(User)
#
#    def __unicode__(self):
#        return u"%s - %s" % (self.show, self.user)

#    def get_absolute_url(self):
#        return "/people/%i/" % self.id
