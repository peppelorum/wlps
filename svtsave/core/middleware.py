from django.conf import settings
import re

class SubdomainsMiddleware:
    def process_request(self, request):
        request.domain = request.META['HTTP_HOST']
        request.subdomain = ''
        parts = request.domain.split('.')

        # blog.101ideas.cz or blog.localhost:8000
        if len(parts) == 3 or (re.match("^localhost", parts[-1]) and len(parts) == 2):
            request.subdomain = parts[0]
            request.domain = '.'.join(parts[1:])

        # set the right urlconf
        if request.subdomain == 'sys':
            request.urlconf = 'svtsave.sys_urls'
#        else:
#            request.urlconf = 'web.urls'